FROM ubuntu:24.04

# install gnupg (to be able to use apt-key) and tzdata (otherwise, the next package install will prompt for timezone info)
RUN apt-get -y update && DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends gnupg tzdata

# install the packages we need from Ubuntu
RUN apt-get -y update \
  && apt-get install --no-install-recommends -y racket imagemagick librsvg2-bin fonts-cantarell fonts-inter git openssl nodejs npm libsqlite3-0 curl ca-certificates latexmk texlive-latex-recommended texlive-latex-extra texlive-humanities texlive-luatex texlive-lang-japanese texlive-plain-generic texlive-pictures cargo fonts-noto

# install Rust-language executables
RUN cargo install svg2pdf-cli

# install necessary Racket libraries
RUN raco pkg install --deps search-auto --no-docs pollen pollen-count base64-lib && \
  raco pollen test && \
  raco setup -p --no-docs;
